from setuptools import setup

APP = ['app.py']
DATA_FILES = ['images']
OPTIONS = {
    'argv_emulation': False,
    'iconfile': 'images/mangrove.icns',
    'plist': {
        'CFBundleShortVersionString': '0.2.0',
        'LSUIElement': True,
    },
    'packages': ['rumps', 'kubernetes'],
}

setup(
    app=APP,
    name='MangroBar',
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'], install_requires=['rumps', 'kubernetes']
)
