import rumps
from collections import defaultdict
from kubernetes import client, config


class MangroBar(object):
    cluster = "a.mangrove.aws.jungle.ai"
    rates = ["2s", "3s", "5s", "10s", "30s"]
    reminders = ["1m", "5m", "15m", "30m", "1h", "2h", "Stop!"]
    icon_path = "images/mangrove.png"

    def __init__(self):
        self.app = rumps.App("Mangrove Bar", icon=self.icon_path)
        self.reminder = rumps.MenuItem(title="Set Reminder")
        self.rate = rumps.MenuItem(title="Set Refresh Rate")

        # Kubernetes client
        config.load_kube_config()
        contexts = config.list_kube_config_contexts()[0]
        for context in contexts:
            if context["name"] == self.cluster:
                self.namespace = context["context"]["namespace"]

        self.client = client.CoreV1Api()

        # Setup app
        self.app.menu = [self.reminder, self.rate]
        self.setup()

        # Set Timers
        self.rate_timer = rumps.Timer(self.on_update, 5)
        self.rate_timer.start()

        self.reminder_timer = rumps.Timer(self.notification, 5)

    def setup(self):
        for rate in self.rates:
            self.rate.add(rumps.MenuItem(title=f"{rate}", callback=self.update_rate))

        for reminder in self.reminders:
            self.reminder.add(
                rumps.MenuItem(title=reminder, callback=self.update_reminder)
            )

    def update_rate(self, rate):
        self.rate_timer.interval = self.convert_time(rate.title)

    def update_reminder(self, reminder):
        if reminder.title == "Stop!":
            self.reminder_timer.stop()
        else:
            self.reminder_timer.interval = self.convert_time(reminder.title)

            if not self.reminder_timer.is_alive():
                self.reminder_timer.start()

    def on_update(self, sender):
        # Clean
        for k, v in self.app.menu.items():
            if k not in ["Set Reminder", "Set Refresh Rate", "Quit"]:
                del self.app.menu[k]

        # Update
        self.pod_list = self.client.list_namespaced_pod(self.namespace).items
        workers = defaultdict(int)

        if len(self.pod_list):
            for pod in self.pod_list:
                if pod.metadata.name == "scheduler":
                    self.app.menu.insert_before(
                        "Quit", rumps.MenuItem(title=f"Scheduler: {pod.status.phase}")
                    )
                else:
                    workers[pod.status.phase] += 1

            if len(workers):
                self.app.menu.insert_before("Quit", rumps.MenuItem(title="Workers:"))
                for k, v in workers.items():
                    self.app.menu.insert_before(
                        "Quit", rumps.MenuItem(title=f"\t{k}: {v}")
                    )
        else:
            self.app.menu.insert_before(
                "Quit", rumps.MenuItem(title="You are not registered!")
            )

    def notification(self, sender):
        if len(self.pod_list):
            rumps.notification(
                "Reminder!",
                f"You have {len(self.pod_list)} pods running!",
                "Don't forget to deregister after you finish your work!",
                icon=self.icon_path,
            )
        else:
            rumps.notification(
                "Reminder!", "You are clean.", "Good job!", icon=self.icon_path
            )

    def convert_time(self, time):
        num = int(time[:-1])
        if time.endswith("s"):
            return num
        elif time.endswith("m"):
            return num * 60
        elif time.endswith("h"):
            return num * 60 * 60

    def run(self):
        self.app.run()


if __name__ == "__main__":
    app = MangroBar()
    app.run()
