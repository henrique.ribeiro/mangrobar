# MangroBar
[![Supported Python versions](https://img.shields.io/badge/python-3.7-blue)]() [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black) 


## What?

A simple app that sits on your menu bar, that allows you to get informed on the number of workers that you have running on mangrove. You can also set a reminder to warn you if you have workers running.

<p align="center">
  <img  height="400" src="images/screenshot.png">
</p>

## How to download?

**[➡️ DOWNLOAD HERE](https://www.dropbox.com/s/c9o9rtkjcfqukrg/MangroBar.zip?dl=0)**

*⚠️ After unzipping and hitting "open", If you are having problems see [this link](https://support.apple.com/en-us/HT202491).*


## How to run?

```
python app.py
```

## How to build?

You need to install py2app package

```
pip install py2app
```

 and then run:

```python
python setup.py py2app
```

*Note: Mac Only (tested on Catalina (yikes!))*

## How?
Python 3, [rumps](https://rumps.readthedocs.io/en/latest/index.html) and [python kubernetes-client](https://github.com/kubernetes-client/python).

## Why? 

Why not? 🤷

